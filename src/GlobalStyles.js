import { injectGlobal } from "styled-components";

const GlobalStyles = () => (
    injectGlobal`
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box; 
            font-family: 'Roboto', sans-serif;
        }

        body {
            color: #47525d;
            font-size: 16px;
            line-height: 1.65;
            font-weight: 300;
            background-color: #fff;
        }

        h1 {
            font-size: 48px;
            margin-bottom: 100px;
            text-align: center;
        }

        h2 {
            font-size: 26px;
            padding-top: 3px;
            padding-bottom: 10px;
            padding-left: 30px;
        }

        p {
            font-size: 18px;
            color: #3d464d;
            line-height: 30px;
            padding-right: 65px;
        }

        html, body {
            height: 100vh;
            overflow-x: hidden;
        }

        a {
            padding-left: 30px;
            padding-bottom: 6px;
            color: inherit;
        }
    `

);

export default GlobalStyles;

