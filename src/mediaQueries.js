import { css } from "styled-components";

// =============== MEASUREMENTS ============= //

// 1. smallPhones - smartphones, iphone, portrait 480x320 phones
// 2. phones -  portrait e-readers (Nook/Kindle), smaller tablets @ 600 or @ 640 wide
// 3. smallTablets - portrait tablets, portrait iPad, landscape e-readers, landscape 800x480 or 854x480 phones 
// 4. tablets - tablet, landscape iPad, lo-res laptops ands desktops
// 5. desktops - big landscape tablets, laptops, and desktops 
// 6. giants - hi-res laptops and desktops

// =============== MEASUREMENTS ============= //

const sizes = {
    giants: 1170,
    desktops: 1025,
    tablets: 961,
    smallTablets: 641,
    phones: 481,
    smallPhones: 320
};

const media = Object.keys(sizes).reduce((accumulator, label) => {
    const emSize = sizes[label] / 16;
    accumulator[label] = (...args) => css`
        @media (min-width: ${emSize}em) {
            ${css(...args)};
        }
    `;
    return accumulator;
}, {});

export default media;

