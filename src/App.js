import React, { Component } from "react";
import GlobalStyles from "./GlobalStyles";
import { Flex, Box } from "grid-styled";
import styled from "styled-components";
import media from "./mediaQueries";
import image_1 from "./img/image-1.png";
import image_2 from "./img/image-2.png";
import image_3 from "./img/image-3.png";
import image_4 from "./img/image-4.png";
import image_5 from "./img/image-5.png";

console.dir(media);

GlobalStyles();

const Container = styled(Box)`
    max-width: 990px;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
`;

const Image = styled.img`
    ${media.smallPhones`
        max-width: 350px;
        height: auto;
    `};
    ${media.desktops`
        max-width: 400px;
        height: auto;
    `};
`;

const Desktoph2 = styled.h2`
    ${media.smallPhones`
        display: none;
    `};
    ${media.desktops`
        display: inline-block;
    `};
`;

const MobileH2 = styled.h2`
    ${media.smallPhones`
        display: inline-block;
        padding-left: 0;
    `};
    ${media.desktops`
        display: none;
        padding-left: 30px;
    `};
`;

const LearnMore = styled.a`
    ${media.smallPhones`
        text-align: center;
        display: block;
    `};
    ${media.desktops`
        text-align: left;
        display: inline-block;
    `};
`;

const ImageBox = styled(Box)`
    ${media.smallPhones`
        order: -1;
    `};
    ${media.desktops`
        order: 0;
    `};
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const MobileParagraph = styled.p`
    ${media.smallPhones`
        text-align: center;
        padding: 30px;
    `};
    ${media.desktops`
        text-align: left;
    `};
`;

const MainTitle = styled(Box)`
    order: -2;
`;

const FooterFlex = styled(Flex)`
    background: #f6f9fc;
    padding: 50px 20px 10px 20px;
    min-height: 300px; 
`;

const FooterBox = styled(Box)`
    display: flex;
    flex-direction: column;
`;

const IconFooter = styled.img`
    width: 36px;
    height: auto;
    margin-right: 15px;
`;

const IconTextContainer = styled.div`
    display: flex;
    width: 100%;
    padding: 15px 30px;
    align-items: center;
`;

const CompanyLogo = styled.img`
    height: auto;
    width: 150px;
`;

class App extends Component {
    render() {
        return (
            <Flex wrap="wrap" direction="column">
                <Container>
                    <Flex is="section" wrap="wrap" mb={30}>
                        <MainTitle w={[ 1 ]}><h1>MAIN TITLE</h1></MainTitle>
                        <Box w={[ 1, 1, 1, 1/2 ]} px={2} pt={15}>
                            <Desktoph2>A Random Title</Desktoph2>
                            <MobileParagraph>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Phasellus fringilla libero non posuere laoreet.
                                Proin pharetra arcu non tincidunt viverra.
                                Quisque dapibus sapien at diam faucibus, non fringilla libero scelerisque.
                                Sed leo erat, dapibus eu odio et, facilisis congue dui.
                                In a erat vitae lectus accumsan lacinia a non justo. Proin vel enim leo.
                                Aenean vestibulum, libero eget lobortis interdum, felis nunc pulvinar nunc, a faucibus purus magna quis ipsum.
                                Nulla eget facilisis sem, in aliquet nibh. In sit amet consectetur leo.
                            </MobileParagraph>
                            <LearnMore href="#">Saiba mais</LearnMore>
                        </Box>
                        <ImageBox w={[ 1, 1, 1, 1/2 ]} px={2}>
                            <MobileH2>Mobile Title</MobileH2>
                            <Image src={image_1} />
                        </ImageBox>
                    </Flex>
                    <Flex is="section" wrap="wrap" mb={30}>
                        <ImageBox w={[ 1, 1, 1, 1/2 ]} px={2}>
                            <MobileH2>Mobile Title</MobileH2>
                            <Image src={image_2} />
                        </ImageBox>
                        <Box w={[ 1, 1, 1, 1/2 ]} px={2} pt={15}>
                            <Desktoph2>A Random Title</Desktoph2>
                            <MobileParagraph>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus fringilla libero non posuere laoreet.
                            Proin pharetra arcu non tincidunt viverra.
                            Quisque dapibus sapien at diam faucibus, non fringilla libero scelerisque.
                            Sed leo erat, dapibus eu odio et, facilisis congue dui.
                            In a erat vitae lectus accumsan lacinia a non justo. Proin vel enim leo.
                            Aenean vestibulum, libero eget lobortis interdum, felis nunc pulvinar nunc, a faucibus purus magna quis ipsum.
                            Nulla eget facilisis sem, in aliquet nibh. In sit amet consectetur leo.
                            </MobileParagraph>
                            <LearnMore href="#">Saiba mais</LearnMore>
                        </Box>
                    </Flex>
                    <Flex is="section" wrap="wrap" mb={30}>
                        <Box w={[ 1, 1, 1, 1/2 ]} px={2} pt={15}>
                            <Desktoph2>A Random Title</Desktoph2>
                            <MobileParagraph>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus fringilla libero non posuere laoreet.
                            Proin pharetra arcu non tincidunt viverra.
                            Quisque dapibus sapien at diam faucibus, non fringilla libero scelerisque.
                            Sed leo erat, dapibus eu odio et, facilisis congue dui.
                            In a erat vitae lectus accumsan lacinia a non justo. Proin vel enim leo.
                            Aenean vestibulum, libero eget lobortis interdum, felis nunc pulvinar nunc, a faucibus purus magna quis ipsum.
                            Nulla eget facilisis sem, in aliquet nibh. In sit amet consectetur leo.
                            </MobileParagraph>
                            <LearnMore href="#">Saiba mais</LearnMore>
                        </Box>
                        <ImageBox w={[ 1, 1, 1, 1/2 ]} px={2}>
                            <MobileH2>Mobile Title</MobileH2>
                            <Image src={image_3} />
                        </ImageBox>
                    </Flex>
                    <Flex is="section" wrap="wrap" mb={30}>
                        <ImageBox w={[ 1, 1, 1, 1/2 ]} px={2}>
                            <MobileH2>Mobile Title</MobileH2>
                            <Image src={image_4} />
                        </ImageBox>
                        <Box w={[ 1, 1, 1, 1/2 ]} px={2} pt={15}>
                            <Desktoph2>A Random Title</Desktoph2>
                            <MobileParagraph>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Phasellus fringilla libero non posuere laoreet.
                        Proin pharetra arcu non tincidunt viverra.
                        Quisque dapibus sapien at diam faucibus, non fringilla libero scelerisque.
                        Sed leo erat, dapibus eu odio et, facilisis congue dui.
                        In a erat vitae lectus accumsan lacinia a non justo. Proin vel enim leo.
                        Aenean vestibulum, libero eget lobortis interdum, felis nunc pulvinar nunc, a faucibus purus magna quis ipsum.
                        Nulla eget facilisis sem, in aliquet nibh. In sit amet consectetur leo.
                            </MobileParagraph>
                            <LearnMore href="#">Saiba mais</LearnMore>
                        </Box>
                    </Flex>
                    <Flex is="section" wrap="wrap" mb={30}>
                        <Box w={[ 1, 1, 1, 1/2 ]} px={2}  pt={15}>
                            <Desktoph2>A Random Title</Desktoph2>
                            <MobileParagraph>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Phasellus fringilla libero non posuere laoreet.
                        Proin pharetra arcu non tincidunt viverra.
                        Quisque dapibus sapien at diam faucibus, non fringilla libero scelerisque.
                        Sed leo erat, dapibus eu odio et, facilisis congue dui.
                        In a erat vitae lectus accumsan lacinia a non justo. Proin vel enim leo.
                        Aenean vestibulum, libero eget lobortis interdum, felis nunc pulvinar nunc, a faucibus purus magna quis ipsum.
                        Nulla eget facilisis sem, in aliquet nibh. In sit amet consectetur leo.
                            </MobileParagraph>
                            <LearnMore href="#">Saiba mais</LearnMore>
                        </Box>
                        <ImageBox w={[ 1, 1, 1, 1/2 ]} px={2}>
                            <MobileH2>Mobile Title</MobileH2>
                            <Image src={image_5} />
                        </ImageBox>
                    </Flex>
                   
                </Container>
                <FooterFlex>
                    <Container>
                        <Flex wrap="wrap">
                            <FooterBox w={[ 1 ]}>
                                <IconTextContainer>
                                    <CompanyLogo src="/img/Logo-Dark.svg"/>
                                </IconTextContainer>
                                <IconTextContainer>
                                    <IconFooter src="/img/location.svg" />
                                    <span>
                                    Av. Prof. Lineu Prestes, 2242, Cietec.<br />
                                    Sala 207.
                                    São Paulo São Paulo
                                    05508-000
                                    Brasil
                                    </span>
                                </IconTextContainer>
                                <IconTextContainer>
                                    <IconFooter src="/img/mail.svg" />
                                    <span>synthetic@mail.com</span>
                                </IconTextContainer>
                                <IconTextContainer>
                                    <span>©2017 Synthetic</span>
                                </IconTextContainer>
                                
                            </FooterBox>
                        </Flex>
                    </Container>
                </FooterFlex>
            </Flex>
        );
    }
}

export default App;
